package fprieto.ksubakamovies.ui.view;

import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.R;
import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.ui.presenter.Presenter;

public class MovieDetailActivity extends BaseActivity {

    @Bind(R.id.iv_movie_background)
    ImageView mComicBackground;
    @Bind(R.id.iv_movie_thumbnail)
    ImageView mComicThumbnail;

    @Bind(R.id.tv_title)
    TextView mTitle;
    @Bind(R.id.tv_year)
    TextView mYear;
    @Bind(R.id.tv_rated)
    TextView mRated;
    @Bind(R.id.tv_released)
    TextView mReleased;
    @Bind(R.id.tv_duration)
    TextView mDuration;
    @Bind(R.id.tv_genre)
    TextView mGenre;
    @Bind(R.id.tv_actors)
    TextView mActors;
    @Bind(R.id.tv_language)
    TextView mLanguage;
    @Bind(R.id.tv_country)
    TextView mCountry;
    @Bind(R.id.tv_awards)
    TextView mAwards;
    @Bind(R.id.tv_plot)
    TextView mPlot;


    @Bind(R.id.ll_title)
    LinearLayout mLinearTitle;
    @Bind(R.id.ll_year)
    LinearLayout mLinearYear;
    @Bind(R.id.ll_rated)
    LinearLayout mLinearRated;
    @Bind(R.id.ll_released)
    LinearLayout mLinearReleased;
    @Bind(R.id.ll_duration)
    LinearLayout mLinearDuration;
    @Bind(R.id.ll_genre)
    LinearLayout mLinearGenre;
    @Bind(R.id.ll_actors)
    LinearLayout mLinearActors;
    @Bind(R.id.ll_language)
    LinearLayout mLinearLanguage;
    @Bind(R.id.ll_country)
    LinearLayout mLinearCountry;
    @Bind(R.id.ll_awards)
    LinearLayout mLinearAwards;
    @Bind(R.id.ll_plot)
    LinearLayout mLinearPlot;

    @Bind(R.id.progress_bar)
    ContentLoadingProgressBar mProgressBar;

    private Movie mMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMovie = (Movie) getIntent().getExtras().getParcelable("MOVIE");
        initializeDagger();
        initFields();
    }

    /**
     * It initializes every field in the view. If there are blank fields (due to there is no API data for that field),
     * the field is hidden
     */
    private void initFields() {
        Picasso.with(this).load(mMovie.Poster()).fit().centerInside().into(mComicThumbnail);
        Picasso.with(this).load(mMovie.Poster()).fit().into(mComicBackground);
        if (mMovie.Title() != null && !mMovie.Title().isEmpty()) {
            mTitle.setText(mMovie.Title());
        } else {
            mLinearTitle.setVisibility(View.GONE);
        }

        if (mMovie.Year() != null && !mMovie.Year().isEmpty()) {
            mYear.setText(mMovie.Year());
        } else {
            mLinearYear.setVisibility(View.GONE);
        }

        if (mMovie.Rated() != null && !mMovie.Rated().isEmpty()) {
            mRated.setText(mMovie.Rated());
        } else {
            mLinearRated.setVisibility(View.GONE);
        }

        if (mMovie.Released() != null && !mMovie.Released().isEmpty()) {
            mReleased.setText(mMovie.Released());
        } else {
            mLinearReleased.setVisibility(View.GONE);
        }

        if (mMovie.Runtime() != null && !mMovie.Runtime().isEmpty()) {
            mDuration.setText(mMovie.Runtime());
        } else {
            mLinearDuration.setVisibility(View.GONE);
        }

        if (mMovie.Genre() != null && !mMovie.Genre().isEmpty()) {
            mGenre.setText(mMovie.Genre());
        } else {
            mLinearGenre.setVisibility(View.GONE);
        }

        if (mMovie.Actors() != null && !mMovie.Actors().isEmpty()) {
            mActors.setText(mMovie.Actors());
        } else {
            mLinearActors.setVisibility(View.GONE);
        }

        if (mMovie.Language() != null && !mMovie.Language().isEmpty()) {
            mLanguage.setText(mMovie.Language());
        } else {
            mLinearLanguage.setVisibility(View.GONE);
        }

        if (mMovie.Country() != null && !mMovie.Country().isEmpty()) {
            mCountry.setText(mMovie.Country());
        } else {
            mLinearCountry.setVisibility(View.GONE);
        }

        if (mMovie.Awards() != null && !mMovie.Awards().isEmpty()) {
            mAwards.setText(mMovie.Awards());
        } else {
            mLinearAwards.setVisibility(View.GONE);
        }

        if (mMovie.Plot() != null && !mMovie.Plot().isEmpty()) {
            mPlot.setText(mMovie.Plot());
        } else {
            mLinearPlot.setVisibility(View.GONE);
        }
        hideLoading();
    }

    /**
     * It injects this Activity to Dagger
     */
    private void initializeDagger() {
        ((KsubakaMoviesApplication) getApplication()).component().inject(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_movie_detail;
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {
        mProgressBar.hide();
    }


}
