package fprieto.ksubakamovies.ui.view.adapter;


import fprieto.ksubakamovies.model.Movie;

public interface MovieClickListener {
        void onMovieSelected(Movie movie);
}
