package fprieto.ksubakamovies.ui.view.adapter;

import fprieto.ksubakamovies.ui.view.adapter.holder.MoviesViewHolder;

public interface MovieListener {
        void onThumbnailClick(MoviesViewHolder position);
}
