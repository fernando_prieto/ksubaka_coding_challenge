package fprieto.ksubakamovies.ui.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fprieto.ksubakamovies.R;
import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.ui.view.adapter.holder.MoviesViewHolder;


public class MoviesAdapter extends RecyclerView.Adapter<MoviesViewHolder> implements MovieListener {

    private LayoutInflater mInflater;
    private List<Movie> mMovies;
    private Context mContext;
    private MovieClickListener mMovieClickListener;


    public MoviesAdapter(@NonNull Context context, @NonNull MovieClickListener movieClickListener) {
        mMovieClickListener = movieClickListener;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mMovies = new ArrayList<>();
    }

    /**
     * Method used when I need to revome all elements in the adapter and add the filtered elements
     *
     * @param collection
     */
    public void filterComics(@NonNull Collection<Movie> collection) {
        mMovies = new ArrayList<>();
        mMovies.addAll(collection);
        notifyDataSetChanged();
    }

    /**
     * Method used when I need to add more elements to the adapter (so I swiped down to get more
     * elements from the API
     *
     * @param collection
     */
    public void addMovies(@NonNull Collection<Movie> collection) {
        mMovies.addAll(collection);
        notifyDataSetChanged();
    }

    public void addMovie(@NonNull Movie movie) {
        mMovies.add(movie);
        notifyDataSetChanged();
    }

    public void clearMovies(){
        mMovies.clear();
    }


    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MoviesViewHolder holder = new MoviesViewHolder(mInflater.inflate(R.layout.adapter_movie, parent, false), this);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolder holder, final int position) {
        Movie movie = mMovies.get(position);
        Picasso.with(mContext).load(movie.Poster()).fit().centerCrop().into(holder.thumbnail);
        holder.movieName.setText(movie.Title());

    }


    @Override
    public int getItemCount() {
        if (mMovies == null) {
            return 0;
        }
        return mMovies.size();
    }

    @Override
    public void onThumbnailClick(MoviesViewHolder position) {
        mMovieClickListener.onMovieSelected(mMovies.get(position.getAdapterPosition()));
    }

}
