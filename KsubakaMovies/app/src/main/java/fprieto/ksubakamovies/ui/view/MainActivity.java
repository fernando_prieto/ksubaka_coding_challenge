package fprieto.ksubakamovies.ui.view;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.R;
import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.ui.presenter.MoviesPresenter;
import fprieto.ksubakamovies.ui.view.adapter.MovieClickListener;
import fprieto.ksubakamovies.ui.view.adapter.MoviesAdapter;
import fprieto.ksubakamovies.usecase.GetMovies;


public class MainActivity extends BaseActivity implements MoviesPresenter.View, MovieClickListener {

    @Bind(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @Bind(R.id.tv_empty_case)
    TextView mEmptyCase;


    @Bind(R.id.fab_createevent)
    FloatingActionButton mFab;

    @Bind(R.id.swipe_refresh_layout)
    SwipyRefreshLayout mSwipeRefreshLayout;

    private EditText mSearchGeneral;
    private EditText mSearchByTitle;
    private TextView mPagesCount;
    private MoviesPresenter mPresenter;
    private MoviesAdapter mAdapter;
    private Movie mResultData;
    private Dialog mDialog;
    private int page;
    private int totalResults;
    private String lastSearched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        mPresenter = createPresenter();
        initializePresenter();
        mPresenter.initialize();
        initFields();

    }

    /**
     * It initializes every field in the view
     */
    private void initFields() {

        mAdapter = new MoviesAdapter(this, this);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        //It makes refreshing the recyclerview when user swipes down at the bottom of the list
        mSwipeRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                if(mAdapter.getItemCount()<totalResults){
                    page++;
                    mPresenter.getMovies(lastSearched, page);
                }
            }
        });
    }

    /**
     * It creates a MoviesPresenter
     *
     * @return the MoviesPresenter created
     */
    public MoviesPresenter createPresenter() {
        return new MoviesPresenter(new GetMovies(KsubakaMoviesApplication.component().webServicesComponent().moviesCall()));
    }

    /**
     * It sets the current View to the MoviesPresenter
     */
    private void initializePresenter() {
        mPresenter.setView(this);
    }

    /**
     * It inyects the Activity to Dagger
     */
    private void initializeDagger() {
        ((KsubakaMoviesApplication) getApplication()).component().inject(this);
    }

    @OnClick(R.id.fab_createevent)
    public void fabEvent() {
        showDialog();
    }


    /**
     * It shows a custom Dialog
     */
    private void showDialog() {
        mDialog = new Dialog(this);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.setContentView(R.layout.dialog_custom);
        mDialog.setTitle(getResources().getString(R.string.dialog_title));

        mSearchGeneral = (EditText) mDialog.findViewById(R.id.et_search);
        mSearchByTitle = (EditText) mDialog.findViewById(R.id.et_search_title);

        mSearchByTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAdapter.clearMovies();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mSearchGeneral.getText().toString().length() > 1) {
                    mPresenter.getMovieByTitle(mSearchByTitle.getText().toString());
                }
            }
        });

        mSearchGeneral.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mAdapter.clearMovies();
                page = 1;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mSearchGeneral.getText().toString().length() > 1) {
                    mPresenter.getMovies(mSearchGeneral.getText().toString(), page);
                    lastSearched = mSearchGeneral.getText().toString();
                }
            }
        });

        mDialog.show();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    /**
     * Callback to receive a Movie searched by title
     * @param resultData : the movie found
     */
    @Override
    public void onMovieFound(Movie resultData) {
        mResultData = resultData;
        mAdapter.addMovie(resultData);
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
        hideEmptyCase();
        hideLoading();
    }

    /**
     * Callback to receive a list o movies searched by any word
     * @param movies : they come paged with 10 elements each time
     * @param totalResults : the total number of movies found
     */
    @Override
    public void onMoviesByTitleFound(List<Movie> movies,int totalResults) {
        mAdapter.addMovies(movies);
        mAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(false);
        this.totalResults = totalResults;
        hideEmptyCase();
        hideLoading();
    }


    @Override
    public void showEmptyCase() {
        mEmptyCase.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyCase() {
        mEmptyCase.setVisibility(View.GONE);
    }


    /**
     * Method to go on Movie Detail after clicking a Movie in the RecyclerView
     * @param movie
     */
    @Override
    public void onMovieSelected(Movie movie) {
        Intent i = new Intent(MainActivity.this, MovieDetailActivity.class);
        i.putExtra("MOVIE", movie);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }
}
