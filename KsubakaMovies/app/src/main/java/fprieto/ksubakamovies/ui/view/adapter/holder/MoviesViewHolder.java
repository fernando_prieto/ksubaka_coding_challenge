package fprieto.ksubakamovies.ui.view.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import butterknife.Bind;
import butterknife.ButterKnife;
import fprieto.ksubakamovies.R;
import fprieto.ksubakamovies.ui.view.adapter.MovieListener;

public class MoviesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @Bind(R.id.iv_movie_thumbnail)
    public ImageView thumbnail;

    @Bind(R.id.tv_movie_name)
    public TextView movieName;
    private MovieListener mMovieListener;

    public MoviesViewHolder(final View itemView, MovieListener movieListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mMovieListener = movieListener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mMovieListener.onThumbnailClick(this);
    }

}
