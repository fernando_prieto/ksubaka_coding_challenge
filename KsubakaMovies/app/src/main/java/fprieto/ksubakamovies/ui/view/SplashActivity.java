package fprieto.ksubakamovies.ui.view;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.R;
import fprieto.ksubakamovies.ui.view.widget.AnimationDrawableCallback;

public class SplashActivity extends BaseActivity {

    @Bind(R.id.iv_background)
    ImageView background;

    @Bind(R.id.iv_ksubaka)
    ImageView iconKsubaka;

    @Bind(R.id.iv_logo)
    ImageView mLogo;
    Timer timer;
    MyTimerTask myTimerTask;
    AnimationDrawable frameAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        loadAnimations();
    }


    /**
     * It the first animation (the cartoon flexing)
     */
    private void loadAnimations() {
        frameAnimation = (AnimationDrawable) mLogo.getDrawable();
        frameAnimation.setVisible(true, true);


        mLogo.post(
                new Runnable() {
                    @Override
                    public void run() {
                        frameAnimation.start();
                    }
                });

        //Calculate the total duration
        int duration = 0;
        for (int i = 0; i < frameAnimation.getNumberOfFrames(); i++) {
            duration += frameAnimation.getDuration(i);
        }

        timer = new Timer();
        myTimerTask = new MyTimerTask();
        timer.schedule(myTimerTask, duration);
    }

    /**
     * Method used to go to MainActivity
     */
    private void goMainActivity() {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }


    /**
     * Method used to show the Ksubaka title (with hidden to visible animation) and also yellow
     * background appears gradually
     */
    private void startKsubakaTitle() {
        iconKsubaka.setVisibility(View.VISIBLE);
        Animation showAnimation = AnimationUtils.loadAnimation(this, R.anim.hidden_to_visible);

        showAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                background.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.visible_to_hidden));
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                goMainActivity();
                background.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iconKsubaka.startAnimation(showAnimation);

    }

    /**
     * It injects this Activity to Dagger
     */
    private void initializeDagger() {
        ((KsubakaMoviesApplication) getApplication()).component().inject(this);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    /**
     * Inner class used to stop animation and start the title animation after the photograms have
     * been showed
     */
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            timer.cancel();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    frameAnimation.stop();
                    mLogo.setVisibility(View.GONE);
                    startKsubakaTitle();
                }
            });
        }
    }

}
