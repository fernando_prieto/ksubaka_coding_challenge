package fprieto.ksubakamovies.ui.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.ui.presenter.Presenter;

public abstract class BaseActivity extends AppCompatActivity implements Presenter.View {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initializeButterKnife();
        initializeDagger();
    }

    public abstract int getLayoutId();

    private void initializeDagger() {
        ((KsubakaMoviesApplication) getApplication()).component().inject(this);
    }

    private void initializeButterKnife() {
        ButterKnife.bind(this);
    }

}
