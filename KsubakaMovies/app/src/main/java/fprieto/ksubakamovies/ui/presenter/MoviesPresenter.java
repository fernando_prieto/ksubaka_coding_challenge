package fprieto.ksubakamovies.ui.presenter;


import android.support.annotation.IntegerRes;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import fprieto.ksubakamovies.configuration.Utils;
import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.model.Movies;
import fprieto.ksubakamovies.usecase.GetMovies;
import rx.functions.Action1;


public class MoviesPresenter extends Presenter<MoviesPresenter.View> {
    GetMovies getMovies;

    public MoviesPresenter(@NonNull GetMovies getMovies) {
        this.getMovies = getMovies;
    }

    public void getMovieByTitle(String title) {
        getMovies.getMovies(title).compose(Utils.<Movie>applySchedulers())
                .subscribe(new Action1<Movie>() {
                    @Override
                    public void call(final Movie movie) {
                        if (movie != null && movie.Title() != null) {
                            getView().onMovieFound(movie);
                        } else {
                            getView().showEmptyCase();
                        }
                    }

                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d("MOVIE_RESP", throwable.getMessage());
                    }
                });
    }

    public void getMovies(String title, int page) {
        getMovies.getMoviesByTitle(title, page).compose(Utils.<Movies>applySchedulers())
                .subscribe(new Action1<Movies>() {
                    @Override
                    public void call(final Movies movies) {
                        if (movies != null && movies.Search().size() > 0) {
                            getView().onMoviesByTitleFound(movies.Search(), Integer.parseInt(movies.totalResults()));
                        } else {
                            getView().showEmptyCase();
                        }
                    }

                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.d("MOVIE_RESP_TITL", throwable.getMessage());
                    }
                });
    }


    public interface View extends Presenter.View {

        void onMovieFound(Movie movie);

        void onMoviesByTitleFound(List<Movie> movies, int numMovies);

        void showEmptyCase();

        void hideEmptyCase();

    }
}
