package fprieto.ksubakamovies.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

import java.util.ArrayList;

import fprieto.ksubakamovies.api.AutoGson;


@AutoValue
@AutoGson(autoClass = AutoValue_Movies.class)
public abstract class Movies implements Parcelable {

    @Nullable
    public abstract ArrayList<Movie> Search();

    @Nullable
    public abstract String totalResults();

    @Nullable
    public abstract String Response();


    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder Search(ArrayList<Movie> search);

        public abstract Builder totalResults(String totalResults);

        public abstract Builder Response(String response);

        public abstract Movies build();
    }

    public static Movies create(ArrayList<Movie> search, String totalResults, String response) {
        return new AutoValue_Movies(search, totalResults, response);
    }


}
