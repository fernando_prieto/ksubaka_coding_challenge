package fprieto.ksubakamovies.model;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

import java.util.List;

import fprieto.ksubakamovies.api.AutoGson;


@AutoValue
@AutoGson(autoClass = AutoValue_Movie.class)
public abstract class Movie implements Parcelable {

    @Nullable
    public abstract String Title();

    @Nullable
    public abstract String Year();

    @Nullable
    public abstract String Rated();

    @Nullable
    public abstract String Released();

    @Nullable
    public abstract String Runtime();

    @Nullable
    public abstract String Genre();

    @Nullable
    public abstract String Director();

    @Nullable
    public abstract String Writer();

    @Nullable
    public abstract String Actors();

    @Nullable
    public abstract String Plot();

    @Nullable
    public abstract String Language();

    @Nullable
    public abstract String Country();

    @Nullable
    public abstract String Awards();

    @Nullable
    public abstract String Poster();

    @Nullable
    public abstract String Metascore();

    @Nullable
    public abstract String imdbRating();

    @Nullable
    public abstract String imdbVotes();

    @NonNull
    public abstract String imdbID();

    @Nullable
    public abstract String Type();

    @Nullable
    public abstract String Response();

    @Nullable
    public abstract String Error();


    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder Title(String title);

        public abstract Builder Year(String year);

        public abstract Builder Rated(String rated);

        public abstract Builder Released(String released);

        public abstract Builder Runtime(String runtime);

        public abstract Builder Genre(String genre);

        public abstract Builder Director(String director);

        public abstract Builder Writer(String writer);

        public abstract Builder Actors(String actors);

        public abstract Builder Plot(String plot);

        public abstract Builder Language(String language);

        public abstract Builder Country(String country);

        public abstract Builder Awards(String awards);

        public abstract Builder Poster(String poster);

        public abstract Builder Metascore(String metascore);

        public abstract Builder imdbRating(String imdbRating);

        public abstract Builder imdbVotes(String imdbVotes);

        public abstract Builder imdbID(String imdbID);

        public abstract Builder Type(String type);

        public abstract Builder Response(String response);

        public abstract Builder Error(String error);

        public abstract Movie build();
    }

    public static Movie create(String title, String year, String rated, String released, String runtime, String genre, String director, String writer, String actors, String plot, String language, String country, String awards, String poster, String metascore, String imdbRating, String imdbVotes, String imdbID, String type, String response, String error) {
        return new AutoValue_Movie(title, year, rated, released, runtime, genre, director, writer, actors, plot, language, country, awards, poster, metascore, imdbRating, imdbVotes, imdbID, type, response, error);
    }


}
