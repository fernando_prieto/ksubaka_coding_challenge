package fprieto.ksubakamovies.api;


import java.util.List;

import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.model.Movies;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface MovieCall {
    @GET("/")
    Observable<Movie> getMovie(@Query("t") String t, @Query("y") String y, @Query("plot") String plot, @Query("r") String r);

    @GET("/")
    Observable<Movies> getMoviesByTitle(@Query("s") String s, @Query("page") int page);
}
