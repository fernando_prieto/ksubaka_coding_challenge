package fprieto.ksubakamovies.usecase;


import java.util.List;

import fprieto.ksubakamovies.api.MovieCall;
import fprieto.ksubakamovies.model.Movie;
import fprieto.ksubakamovies.model.Movies;
import rx.Observable;

public class GetMovies {

    MovieCall movieCall;

    public GetMovies(MovieCall movieCall) {
        this.movieCall = movieCall;
    }

    public Observable<Movie> getMovies(String title) {

        return movieCall.getMovie(title, "", "short","json");
    }

    public Observable<Movies> getMoviesByTitle(String title, int page) {

        return movieCall.getMoviesByTitle(title, page);
    }

}
