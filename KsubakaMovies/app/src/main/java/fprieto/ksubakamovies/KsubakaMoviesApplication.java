
package fprieto.ksubakamovies;

import android.app.Application;

import fprieto.ksubakamovies.dagger.ApplicationComponent;
import fprieto.ksubakamovies.dagger.DaggerApplicationComponent;
import fprieto.ksubakamovies.dagger.MainModule;


public class KsubakaMoviesApplication extends Application {

    private static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerApplicationComponent.builder()
                .mainModule(new MainModule(this))
                .build();
        component().inject(this);
    }


    public static ApplicationComponent component() {
        return component;
    }
}
