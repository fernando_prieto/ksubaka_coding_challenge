package fprieto.ksubakamovies.dagger;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.api.AutoValueTypeAdapterFactory;

@Module
public class MainModule {
    private final KsubakaMoviesApplication application;


    private static final String PREFERENCES_NAME = "ksubakamovies_preferences";

    public MainModule(KsubakaMoviesApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }


    @Singleton
    @Provides
    public SharedPreferences preferences() {
        return application.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Singleton
    @Provides
    public Gson providesGson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                .registerTypeAdapterFactory(new AutoValueTypeAdapterFactory()).create();
    }


}
