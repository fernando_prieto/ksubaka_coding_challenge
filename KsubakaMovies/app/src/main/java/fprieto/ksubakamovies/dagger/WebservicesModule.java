package fprieto.ksubakamovies.dagger;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import fprieto.ksubakamovies.api.MovieCall;
import retrofit2.Retrofit;

@Module
public class WebservicesModule {

    @Provides
    public MovieCall providesMoviesCall(@NonNull Retrofit retrofit) {
        return retrofit.create(MovieCall.class);
    }

}
