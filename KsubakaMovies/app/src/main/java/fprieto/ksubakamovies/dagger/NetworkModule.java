package fprieto.ksubakamovies.dagger;


import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private static final String BASE_OMDB_URL = "http://www.omdbapi.com";


    @Provides
    @Singleton
    @NonNull
    Retrofit providesRetrofitApi(@NonNull OkHttpClient okHttpClient, @NonNull Converter.Factory factory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BASE_OMDB_URL)
                .build();
    }

    @NonNull
    @Provides
    @SuppressWarnings("all")
    public OkHttpClient providesOkHttpClient(@NonNull Authenticator authenticator) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .authenticator(authenticator);
        return builder.build();
    }

    @NonNull
    @Provides
    public Authenticator providesAuthenticator(@NonNull Context applicationContext) {
        return new KsubakaMoviesAuthenticator(applicationContext);
    }

    @NonNull
    @Provides
    public Converter.Factory providesConverterFactory(@NonNull Gson gson) {
        return GsonConverterFactory.create(gson);
    }


}
