package fprieto.ksubakamovies.dagger;

import dagger.Subcomponent;
import fprieto.ksubakamovies.api.MovieCall;

@Subcomponent(
        modules = {
                WebservicesModule.class
        }
)
public interface WebServicesComponent {

    MovieCall moviesCall();
}
