package fprieto.ksubakamovies.dagger;

import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Component;
import fprieto.ksubakamovies.KsubakaMoviesApplication;
import fprieto.ksubakamovies.ui.view.BaseActivity;
import fprieto.ksubakamovies.ui.view.MainActivity;
import fprieto.ksubakamovies.ui.view.MovieDetailActivity;
import fprieto.ksubakamovies.ui.view.SplashActivity;


@Singleton
@Component(modules = {MainModule.class ,NetworkModule.class })
public interface ApplicationComponent {

    SharedPreferences preferences();

    void inject(KsubakaMoviesApplication ksubakaMoviesApplication);

    void inject(BaseActivity baseActivity);

    void inject(MainActivity mainActivity);

    void inject(MovieDetailActivity movieDetailActivity);

    void inject(SplashActivity splashActivity);

    WebServicesComponent webServicesComponent();
}
