package fprieto.ksubakamovies.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class KsubakaMoviesAuthenticator implements Authenticator {

    private Context mContext;

    public KsubakaMoviesAuthenticator(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        return null;
    }
}
